//
//  ViewController.swift
//  testOS
//
//  Created by 譚培成 on 2021/3/17.
//

import Cocoa
import SnapKit
import AVKit

class ViewController: NSViewController {
    let playerView = AVPlayerView()
    let textField = NSTextField()
//    private var player: AVPlayer?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()
        
//        "/Users/tanpeicheng/Desktop/resource/SampleVideo_01.mp4"
//        "/Users/tanpeicheng/Desktop/resource/001.MOV"
    }
    override func viewDidAppear() {
        super.viewDidAppear()
        self.view.window?.title = ""
    }
}

extension ViewController: NSTextFieldDelegate {
    func controlTextDidChange(_ obj: Notification) {
        guard textField.stringValue != "" else {
            return
        }
        defer {
            DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
                self.textField.stringValue = ""
            }
        }
        
        let path = textField.stringValue
        print(textField.stringValue)
        let url = URL(fileURLWithPath: path)
        
        if let oldPlayer = playerView.player {
            oldPlayer.pause()
            oldPlayer.replaceCurrentItem(with: AVPlayerItem(url: url))
        }
        
        let player = AVPlayer(url: url)
        playerView.player = player
        player.play()
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: player.currentItem, queue: .main) { _ in
            player.seek(to: CMTime.zero)
            player.play()
        }
        
    }
}

private extension ViewController {
    func configUI() {
        view.wantsLayer = true
        view.layer?.backgroundColor = NSColor.black.cgColor
        
        view.addSubview(textField)
        textField.snp.makeConstraints {
            $0.left.right.bottom.equalToSuperview()
            $0.height.equalTo(30)
        }
        textField.wantsLayer = true
        textField.layer?.backgroundColor = NSColor.lightGray.cgColor
        textField.delegate = self
        textField.textColor = NSColor.black
        
        view.addSubview(playerView)
        playerView.snp.makeConstraints {
            $0.top.left.right.equalToSuperview()
            $0.bottom.equalTo(textField.snp.top)
        }
    }
}
